import{I as b}from"./iconfont-900e007c.js";import{P as d}from"./page-wrapper-bfe2595f.js";import{a4 as u,J as r,o as p,c as g,w as e,u as o,B as s}from"./vendor-10026a69.js";import"./index-7532bb53.js";import"./chunk-bin-ui-next-48d4440b.js";import"./chunk-brace-f547fd93.js";const m={name:"CompIconfont",components:{PageWrapper:d,Iconfont:b},methods:{clickFunc(t){this.$message.info(`\u70B9\u51FB\u4E86\u56FE\u6807:${t}`)}}},_={class:"p10"},f={class:"p10"},z={class:"p10"},k={class:"p10"};function h(t,w,y,C,v,n){const c=r("iconfont"),i=r("b-collapse-wrap"),l=r("b-divider"),a=r("page-wrapper");return p(),g(a,{desc:"\u56FE\u6807\u5B57\u4F53\u7EC4\u4EF6\uFF0C\u57FA\u4E8Eiconfont\u6A21\u5F0F\uFF0C\u6269\u5C55\u7EC4\u4EF6\u5E93\u56FE\u6807\u663E\u793A\uFF0C\u53EF\u8BBE\u7F6E\u989C\u8272\u3001\u80CC\u666F\u8272\u3001\u4E2D\u6587\u540D\u3001\u5927\u5C0F\u7B49"},{default:e(()=>[o(i,{title:"\u57FA\u7840\u56FE\u6807",shadow:"none"},{default:e(()=>[s("div",_,[o(c,{icon:"appstore"}),o(c,{icon:"bulb"}),o(c,{icon:"edit-square"}),o(c,{icon:"tags"}),o(c,{icon:"heart"}),o(c,{icon:"smile"})])]),_:1}),o(i,{title:"\u9644\u5E26\u989C\u8272",shadow:"none"},{default:e(()=>[s("div",f,[o(c,{icon:"appstore",color:"primary"}),o(c,{icon:"bulb",color:"success"}),o(c,{icon:"edit-square",color:"warning"}),o(c,{icon:"tags",color:"info"}),o(c,{icon:"heart",color:"danger"}),o(c,{icon:"smile",color:"#25bbe3"})])]),_:1}),o(i,{title:"\u8BBE\u7F6E\u5927\u5C0F\u548C\u80CC\u666F\u989C\u8272",shadow:"none"},{default:e(()=>[s("div",z,[o(c,{icon:"appstore",color:"primary",size:30,bg:""}),o(c,{icon:"bulb",color:"success",size:30,bg:""}),o(c,{icon:"edit-square",color:"warning",size:30,bg:""}),o(c,{icon:"tags",color:"info",size:30,bg:""}),o(c,{icon:"heart",color:"danger",size:30,bg:""}),o(c,{icon:"smile",color:"#25bbe3",size:30,bg:""}),o(l,{type:"vertical"}),o(c,{icon:"appstore",color:"primary",size:30,bg:"",round:""}),o(c,{icon:"bulb",color:"success",size:30,bg:"",round:""}),o(c,{icon:"edit-square",color:"warning",size:30,bg:"",round:""}),o(c,{icon:"tags",color:"info",size:30,bg:"",round:""}),o(c,{icon:"heart",color:"danger",size:30,bg:"",round:""}),o(c,{icon:"smile",color:"#25bbe3",size:30,bg:"",round:""}),o(l,{type:"vertical"}),o(c,{icon:"appstore",color:"primary",size:40,bg:"",round:""}),o(c,{icon:"bulb",color:"success",size:40,bg:"",round:""}),o(c,{icon:"edit-square",color:"warning",size:40,bg:"",round:""}),o(c,{icon:"tags",color:"info",size:40,bg:"",round:""}),o(c,{icon:"heart",color:"danger",size:40,bg:"",round:""}),o(c,{icon:"smile",color:"#25bbe3",size:40,bg:"",round:""}),o(l,{type:"vertical"}),o(c,{icon:"appstore",color:"primary",size:40}),o(c,{icon:"bulb",color:"success",size:40}),o(c,{icon:"edit-square",color:"warning",size:40}),o(c,{icon:"tags",color:"info",size:40}),o(c,{icon:"heart",color:"danger",size:40}),o(c,{icon:"smile",color:"#25bbe3",size:40})])]),_:1}),o(i,{title:"\u53EF\u70B9\u51FB",shadow:"none"},{default:e(()=>[s("div",k,[o(c,{icon:"appstore",color:"primary",size:30,bg:"",round:"",onClick:n.clickFunc,type:"btn"},null,8,["onClick"]),o(c,{icon:"bulb",color:"success",size:30,bg:"",round:"",onClick:n.clickFunc,type:"btn"},null,8,["onClick"]),o(c,{icon:"edit-square",color:"warning",size:30,bg:"",round:"",onClick:n.clickFunc,type:"btn"},null,8,["onClick"]),o(c,{icon:"tags",color:"info",size:30,bg:"",round:"",onClick:n.clickFunc,type:"btn"},null,8,["onClick"]),o(c,{icon:"heart",color:"danger",size:30,bg:"",round:"",onClick:n.clickFunc,type:"btn"},null,8,["onClick"]),o(c,{icon:"smile",color:"#25bbe3",size:30,bg:"",round:"",onClick:n.clickFunc,type:"btn"},null,8,["onClick"])])]),_:1})]),_:1})}var N=u(m,[["render",h]]);export{N as default};
